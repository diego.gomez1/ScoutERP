from openerp.osv import fields, osv
from openerp.tools import config

class wordpress_configuration(osv.osv_memory):
    _name='wordpress.configuration.settings'
    _description = 'Wordpress Connection Configuration'
    _inherit = 'res.config.settings'
    _rec_name = 'xmlrpc_host'
    _columns = {
        'xmlrpc_host': fields.char('XMLRPC Host', size=255,
                            help="Set the endpoint for the Wordpress XMLRPC connection",
                            required=True),
        'username': fields.char('Username', size=100, required=True),
        'password': fields.char('Password', size=100, required=True)
    }

    def get_default_wordpress(self, cr, uid, fields, context=None):
        key = self.pool.get("ir.config_parameter").get_param(cr, uid, "wordpress.xmlrpc.host") or "http://www.my-host.com/mywordpress/xmlrpc.php"
        user = self.pool.get('ir.config_parameter').get_param(cr, uid, 'wordpress.username') or ""
        password = self.pool.get('ir.config_parameter').get_param(cr, uid, 'wordpress.password')
        return {'xmlrpc_host': key, 'username': user, 'password': password}

    def set_wordpress(self, cr, uid, ids, context=None):
        record = self.browse(cr, uid, ids[0], context)
        key = record["xmlrpc_host"] or ""
        self.pool.get("ir.config_parameter").set_param(cr, uid, "wordpress.xmlrpc.host", key)
        key = record["username"] or ""
        self.pool.get("ir.config_parameter").set_param(cr, uid, "wordpress.username", key)
        key = record["password"] or ""
        self.pool.get("ir.config_parameter").set_param(cr, uid, "wordpress.password", key)


    def execute222(self, cr, uid, ids, context=None):
        conf = self.browse(cr, uid, ids[0], context=context)

        config.set('wordpress_endpoint_host', conf.xmlrpc_host)
        config.set('wordpress_endpoint_auth_username', conf.username)
        config.set('wordpress_endpoint_auth_password', conf.password)


wordpress_configuration()