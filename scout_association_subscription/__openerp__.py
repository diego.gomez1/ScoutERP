# -*- coding: utf-8 -*-
{
    'name': 'Scout Association Subscription',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout members, parents, scouters, evaluation of participants',
    'description': """
Scout Association Subscription
==============================

Allow managing subscription and payments.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_association", "sale"],
    'data': [
        "scout_subscription_period_view.xml",
        "scout_subscription_period_workflow.xml",
        "wizard/subscription_period_payment_wizard_view.xml",
        "scout_participant_view.xml",
        "security/ir.model.access.csv"
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [  ],
    'installable': True,
    'application': True,
    'images': [],
}