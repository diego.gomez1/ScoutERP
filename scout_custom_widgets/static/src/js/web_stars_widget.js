openerp.scout_custom_widgets = function(instance) {

    var raty_path = "/scout_custom_widgets/static/src/img/";

    instance.scout_custom_widgets.StarsRatingWidget = instance.web.form.FieldChar.extend({
        template:"StarsRatingWidget",
        init: function(view,code){
            this._super(view,code);
        },
        initialize_content: function() {
            this._super();

            this.raty = this.$el.raty({score: this.start_rate, path: raty_path});
        },
        render_value: function() {
            // console.log('Render value'+this.get('id'));
            var stringInput = this.get('value');
            this.stars_rate = parseInt(stringInput || 0);
            // console.log(this.stars_rate);

            this.raty.raty('readOnly',false);
            this.raty.raty('score', this.stars_rate);
            this.raty.raty('readOnly',this.get('effective_readonly'));
        }
    });

    instance.scout_custom_widgets.ListStarsRatingWidget = instance.web.list.Column.extend({
        _format: function (row_data, options) {
            var input = parseInt(row_data[this.id].value || 0);
            console.log(row_data[this.id].value, input);

            return _.template(
                '<div data-score="<%-value%>" id="stars<%-id%>"></div><script>$("#stars<%-id%>").raty({path: "'+raty_path+'", score: $("#stars<%-id%>").attr("data-score") });</script>',
                {
                    value: _.str.sprintf("%.0f", input),
                    id: options.id,
                    path: raty_path
                });
        }
    });

    // instance.web.list.widgets.add('stars_rating', 'instance.scout_custom_widgets.StarsRatingWidget');
    instance.web.form.widgets.add('stars_rating', 'instance.scout_custom_widgets.StarsRatingWidget');
    instance.web.list.columns.add('field.stars_rating', 'instance.scout_custom_widgets.ListStarsRatingWidget');
};


