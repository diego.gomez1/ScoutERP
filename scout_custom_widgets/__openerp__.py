# -*- coding: utf-8 -*-
{
    'name': 'Custom Widgets',
    'version': '0.1',
    'category': 'Hidden',
    'summary': 'Widgets for star rating',
    'description': """
Custom Widgets
==============

Stars rating widget
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ['web'],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'installable': True,
    'auto_install': True,
    'js': ['static/src/js/jquery.raty.min.js', 'static/src/js/web_stars_widget.js'],
    'qweb': ['static/src/xml/web_stars_widget.xml'],
}