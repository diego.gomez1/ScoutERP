# -*- coding: utf-8 -*-
# "l10n_es_partner", "l10n_es_toponyms", "l10n_es_account_asset", "account_voucher", "hr_expense"
{
    'name': 'Trotamundos',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout Activities, Games, Songs',
    'description': """
Scout base
==========

This is a customization of the application for fast instalation.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["activities_programming", "scout_association", "scouters_management", "scout_association_health" ],
    'data': [ "data/trotamundos_data.xml", "data/trotamundos_achievements_data.xml" ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
}