# -*- coding: utf-8 -*-
{
    'name': 'Scout Association',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout members, parents, scouters, evaluation of participants',
    'description': """
Scout base
==========

Model and views for the scout members, scouters, parents and families.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_base"],
    'data': [
        "scout_association_view.xml",
        "scout_participant_view.xml",
        "scout_family_view.xml",
        "scout_parent_view.xml",
        "scout_achievement_view.xml",
        "security/scout_security.xml",
        "security/ir.model.access.csv",
        "data/scout_parent_relationship_data.xml"
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [ "demo/scout.family.csv" ],
    'installable': True,
    'application': True,
    'images': [],
}