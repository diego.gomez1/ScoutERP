import openerp
from osv import osv, fields
from openerp import tools
from datetime import date
from time import strptime


class scout_participant(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.participant'
    _description = 'Scout Participant'
    _inherit = 'scout.participant'

    _columns = {
        'blood_group': fields.char("Blood group", size=15),
        'allergies': fields.text('Allergies'),
        'allergies_treatments': fields.text('Allergies treatments'),
        'diseases': fields.text('Diseases'),
        'diseases_treatments': fields.text('Diseases treatments'),
        'special_needs': fields.text('Special needs'),
        'social_security_number': fields.char("Social security number", size=60),
        'social_security_card': fields.binary("Social security card"),
    }
