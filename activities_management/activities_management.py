from osv import osv,fields
import html2text

class scout_activity(osv.Model):
    """
    Scout Activities
    """
    _name = 'scout.activity'
    _description = 'Scout Activities'

    def get_raw_description(self, cr, uid, ids, name, arg, context):
        """
        Escape the html code for the report
        """
        result = {}
        for activity in self.browse(cr, uid, ids, context=context):
            h = html2text.HTML2Text()
            h.body_width = 0
            h.ignore_links = True
            h.ignore_images = True
            h.ignore_emphasis = True
            parsed = h.handle(activity.description)
            result[activity.id] = parsed
        return result

    def _get_ages_ids_default(self, cr, uid, context=None):
        ages_obj = self.pool.get('scout.age')
        ages_ids = ages_obj.search(cr, uid, [], context=context)

        return ages_ids

    _columns = {
        'name': fields.char("Name", size=128,),
        'objectives_ids': fields.one2many('scout.activity.objective', 'activity_id', 'Objectives'),
        'description': fields.html('Description'),
        'youtube_video': fields.char("Youtube Video", size=256,),
        'activity_source': fields.char("Activity source", size=256,),
        'duration_minutes': fields.integer("Duration in minutes"),
        'ages_ids': fields.many2many('scout.age', 'scout_activity_age_rel', 'activity_id', 'age_id', 'Activity for ages'),
        'materials_ids': fields.many2many('scout.activity.material', 'scout_activity_materials_rel', 'activity_id', 'material_id', 'Materials for activity'),
        'materials_description': fields.html('Materials description'),
        'submitted_by_id': fields.many2one('res.partner', 'Submitted by'),
        'vote_ids': fields.one2many('scout.activity.vote', 'activity_id', 'Votes'),
        'score': fields.float('Score', digits=(2,1)),
        'categories_ids': fields.many2many('scout.activity.category', 'scout_activity_category_rel', 'activity_id', 'category_id', 'Activity categories'),
        'locations_ids': fields.many2many('scout.activity.location', 'scout_activity_location_rel', 'activity_id', 'location_id', 'Activity locations'),
        'groupings_ids': fields.many2many('scout.activity.grouping', 'scout_activity_grouping_rel', 'activity_id', 'grouping_id', 'Grouping mode'),
        'attachment_ids': fields.many2many('ir.attachment', 'scout_activity_attachments_rel', 'activity_id', 'attachment_id', 'Attachments'),
        'variations_ids': fields.many2many('scout.activity', 'scout_activity_variations_rel', 'activity1_id', 'activity2_id', 'Variations'),
        'raw_description': fields.function(get_raw_description, type="text", method=True, string='Raw Description')
    }

    _defaults = {
        'ages_ids': _get_ages_ids_default
    }



class scout_activity_material(osv.Model):
    """
    Scout Activity Materials
    """
    _name = 'scout.activity.material'
    _description = 'Scout Activity Material'
    _columns = {
        'name': fields.char("Name", size=128,),
        'description': fields.text('Description')
    }


class scout_activity_objective(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.activity.objective'
    _description = 'Scout Activity Objective'
    _columns = {
        'name': fields.char("Name", size=256,),
        'activity_id': fields.many2one('scout.activity', 'Activity', ondelete='cascade'),
        'description': fields.text('Description')
    }


class scout_activity_category(osv.Model):
    """
    Scout Activity Category
    """
    _name = 'scout.activity.category'
    _description = 'Scout Activity Category'
    _columns = {
        'name': fields.char("Name", size=256,)
    }

class scout_activity_grouping(osv.Model):
    """
    Scout Activity Category
    """
    _name = 'scout.activity.grouping'
    _description = 'Grouping mode'
    _columns = {
        'name': fields.char("Name", size=256,)
    }

class scout_activity_location(osv.Model):
    """
    Scout Activity Category
    """
    _name = 'scout.activity.location'
    _description = 'Activity location'
    _columns = {
        'name': fields.char("Name", size=256,)
    }

class scout_activity_vote(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.activity.vote'
    _description = 'Scout Activity Vote'
    _columns = {
        'name': fields.char("Name", size=256,),
        'activity_id': fields.many2one('scout.activity', 'Activity', ondelete='cascade'),
        'score': fields.float('Score', digits=(2,1)),
        'voted_by_id': fields.many2one('res.partner', 'Voted by')
    }


