# -*- coding: utf-8 -*-
{
    'name': 'Activities Management',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout Activities, Games, Songs',
    'description': """
Activities Management
=====================

This module has been created for managing different kinds of activities like games, songs
arts & crafts, etc.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_base", "scout_custom_widgets"],
    'data': [
        "activities_management_view.xml",
        "data/activity_reports.xml",
        "data/activity_category.xml",
        "data/activity_grouping.xml",
        "data/activity_locations.xml",
        "security/ir.model.access.csv"
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
    'js': ['static/src/js/web_stars_widget.js'],
    'qweb': ['static/src/xml/web_stars_widget.xml'],
}