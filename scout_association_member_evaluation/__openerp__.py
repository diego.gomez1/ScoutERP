# -*- coding: utf-8 -*-
{
    'name': 'Scout Association Member Evaluation',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scout members, parents, scouters, evaluation of participants',
    'description': """
Scout Association Member Evalutaion
===================================

This module allow adding evaluation of members.
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_association", "scouters_management"],
    'data': [
        "scout_participant_view.xml",
        "security/ir.model.access.csv"
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [  ],
    'installable': True,
    'application': True,
    'images': [],
}