import pooler
from osv import fields,osv
import base64
import time
import vobject

from osv import osv
from tools.translate import _
import re
from datetime import datetime


class scout_association_members_edm_wizard(osv.osv_memory):

    _name = "scout.association.members.edm.wizard"
    _description = "Scout association members EDM list Wizard"

    _columns = {
        'date_from': fields.date('Date from', help="Set the starting date to import the participants"),
        'group_number': fields.char('Group Number')
    }
    _defaults = {
        'group_number': '957'
    }

    def act_getfile(self, cr, uid, ids, context={}):
        """
        Action that exports the data into a BOE formated text file
        """
        this = self.browse(cr, uid, ids)[0]
        participants_obj = self.pool.get('scout.participant')
        scouters_obj = self.pool.get('scout.scouter')
        members_edm = self.pool.get('scout.association.members.edm')

        members_existing_ids = members_edm.search(cr, uid, [], context=context)
        members_edm.unlink(cr, uid, members_existing_ids)

        ## Scouters
        active_scouters_ids = scouters_obj.search(cr, uid, [('state', '=', 'active'), ('subscribe_edm', '=', True), ('member_since', '>=', this.date_from)], order='member_number', context=context)
        for scouter in scouters_obj.browse(cr, uid, active_scouters_ids, context):
            dni = '0'
            dni_letter = ''
            if scouter.dni:
                dni_parts = re.search('(X)?(\d*)(\w)?', scouter.dni)
                dni = dni_parts.group(2)
                if dni_parts.group(1):
                    dni_letter = dni_parts.group(1)
                else:
                    dni_letter = dni_parts.group(3)
            birthday = ''
            member_since = ''
            member_end = ''
            birthday = datetime.strptime(scouter.birthday, '%Y-%m-%d').strftime('%d/%m/%Y')
            member_since = datetime.strptime(scouter.member_since, '%Y-%m-%d').strftime('%d/%m/%Y')

            scouting_in_unit = ''
            if scouter.unit_id and scouter.unit_id.age_id:
                scouting_in_unit = scouter.unit_id.age_id.name_edm

            members_edm.create(cr, uid,
                {
                    'group_number': this.group_number,
                    'member_number': '28%s%04d' % (this.group_number, int(scouter.member_number)),
                    'name': scouter.name,
                    'last_name': scouter.last_name,
                    'dni': dni,
                    'dni_letter': dni_letter,
                    'address': "%s %s" % (scouter.street, scouter.street2),
                    'zip': scouter.zip,
                    'locality': scouter.city,
                    'phone': (scouter.phone if scouter.phone else '9'),
                    'mobile': (scouter.mobile if scouter.mobile else '9'),
                    'email': scouter.email,
                    'birthday': birthday,
                    'member_since': member_since,
                    'unit': 'Scouters',
                    'scouting_in_unit': scouting_in_unit,
                    'unit_name': '',
                    'gender': ('Hombre' if scouter.gender == 'm' else 'Mujer')
                })

        ## Participants
        active_members_ids = participants_obj.search(cr, uid, [('state', '=', 'subscribed'), ('subscribe_edm', '=', True), ('member_since', '>=', this.date_from)], order='member_number', context=context)

        for participant in participants_obj.browse(cr, uid, active_members_ids, context):
            dni = '0'
            dni_letter = ''
            if participant.dni:
                dni_parts = re.search('(X)?(\d*)(\w)?', participant.dni)
                dni = dni_parts.group(2)
                if dni_parts.group(1):
                    dni_letter = dni_parts.group(1)
                else:
                    dni_letter = dni_parts.group(3)
            birthday = ''
            member_since = ''
            member_end = ''
            birthday = datetime.strptime(participant.birthday, '%Y-%m-%d').strftime('%d/%m/%Y')
            member_since = datetime.strptime(participant.member_since, '%Y-%m-%d').strftime('%d/%m/%Y')

            members_edm.create(cr, uid,
                {
                    'group_number': this.group_number,
                    'member_number': '28%s%04d' % (this.group_number, int(participant.member_number)),
                    'name': participant.name,
                    'last_name': participant.family_id.name,
                    'dni': dni,
                    'dni_letter': dni_letter,
                    'address': "%s %s" % (participant.family_id.street, participant.family_id.street2),
                    'zip': participant.family_id.zip,
                    'locality': participant.family_id.city,
                    'phone': (participant.family_id.phone if participant.family_id.phone else '9'),
                    'mobile': (participant.family_id.parent_ids[0]['phone'] if participant.family_id.parent_ids[0]['phone'] else '9'),
                    'email': participant.family_id.parent_ids[0].email,
                    'birthday': birthday,
                    'member_since': member_since,
                    'unit': participant.unit_id.age_id.name_edm,
                    'unit_name': participant.unit_id.name,
                    'gender': ('Hombre' if participant.gender == 'm' else 'Mujer')
                })

        members_created_ids = members_edm.search(cr, uid, [], order='member_number', context=context)

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'edm_members_report',
            'datas': {
                'model': 'scout.association.members.edm',
                'ids': members_created_ids
            }
        }



class scout_association_members_edm(osv.osv_memory):

    _name = "scout.association.members.edm"
    _description = "Scout association members EDM list"

    _columns = {
        'group_number': fields.char('Group Number'),
        'member_number': fields.char('Member number'),
        'dni': fields.char('DNI'),
        'dni_letter': fields.char('DNI letter'),
        'name': fields.char('First name'),
        'last_name': fields.char('Last name'),
        'address': fields.char('Address'),
        'zip': fields.char('Zip'),
        'locality': fields.char('Locality'),
        'phone': fields.char('Phone number'),
        'mobile': fields.char('Mobile number'),
        'birthday': fields.char('Birthday'),
        'member_since': fields.char('Member since date'),
        'unit': fields.char('Unit'),
        'unit_name': fields.char('Unit name'),
        'scouting_in_unit': fields.char('Scouting in unit'),
        'gender': fields.char('Gender'),
        'email': fields.char('E-mail'),
        'member_end': fields.char('Member ends')
    }


