from osv import osv, fields


class scout_age(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.age'
    _inherit = 'scout.age'
    _description = 'Scout Activity Vote'
    _columns = {
        'name_edm': fields.char("Name EDM", size=256,)
    }


class scout_participant(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.participant'
    _description = 'Scout Participant'
    _inherit = 'scout.participant'

    _columns = {
        'subscribe_edm': fields.boolean('Subscribe on EDM'),
    }

    _defaults = {
        'subscribe_edm': False
    }


class scout_scouter(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.scouter'
    _description = 'Scout Scouter'
    _inherit = 'scout.scouter'

    _columns = {
        'subscribe_edm': fields.boolean('Subscribe on EDM'),
    }

    _defaults = {
        'subscribe_edm': False
    }