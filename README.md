# ScoutERP #

This set of modules have been developed to work with OpenERP 7.0 and have not been tested yet on Odoo.

ScoutERP allow scout groups to manage their participants information, the planing and evaluation of activities and the payments of activities.

It also contains a set of activities and some tools to export data for the Spanish Association of Scouts (ASDE). 