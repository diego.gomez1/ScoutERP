# -*- coding: utf-8 -*-
{
    'name': 'Scouters Management',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Scouters information',
    'description': """
Scouters management
===================

Let scouters of a group access to the system and access the information of other scouters
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_association"],
    'data': [ "scouters_management_view.xml", "security/ir.model.access.csv" ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [ ],
    'installable': True,
    'application': True,
    'images': [],
}