import openerp
from osv import osv, fields
from openerp import tools
from datetime import date
from time import strptime
from openerp.tools.translate import _
import math


class scout_evaluation_select_template_wizard(osv.osv_memory):
    """
    Scout Subscription
    """
    _name = 'scout.evaluation.template.wizard'
    _columns = {
        'evaluation_id': fields.many2one('scout.activity.programming.evaluation', 'Programming evaluation'),
        'evaluation_template_id': fields.many2one('scout.evaluation.template', 'Evaluation template')
    }

    def select_template(self, cr, uid, ids, context=None):

        evaluation_obj = self.pool.get('scout.activity.programming.evaluation')
        evaluation_value_obj = self.pool.get('scout.evaluation.item.value')
        evaluation_template_obj = self.pool.get('scout.evaluation.template')

        for wiz in self.browse(cr, uid, ids, context):

            evaluation = wiz.evaluation_id
            evaluation_template = wiz.evaluation_template_id

            for evaluation_item in evaluation_template.evaluation_items_ids:
                evaluation_value_obj.create(cr, uid, {
                    'evaluation_item_id': evaluation_item.id,
                    'programming_evaluation_id': evaluation.id
                })
        return {}

scout_evaluation_select_template_wizard();