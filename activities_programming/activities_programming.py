import openerp.exceptions

import datetime

from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

from datetime import date
from time import strptime
import html2text

import logging

_logger = logging.getLogger(__name__)


class scout_activity_programming(osv.Model):
    """
    Scout Activity Programming
    """
    _name = 'scout.activity.programming'
    _description = 'Scout Activities Programming'
    _inherit = ['mail.thread']

#    def default_get(self, cr, uid, fields_list, context=None):
#        context = context or {}
#        obj_scouter = self.pool.get('scout.scouter')
#        scouter_ids = obj_scouter.search(cr, uid, [('user_id', '=', uid)])
#
#        data = super(scout_activity_programming,self).default_get(cr, uid, fields_list, context)
#
#        for scouter in obj_scouter.browse(cr, uid, scouter_ids, context=context):
#            data['user_unit_id'] = scouter.unit_id.id
#
#        return data

    def create(self, cr, uid, vals, context=None):
        print vals
        obj_post = self.pool.get('wordpress.post')
        post_id = obj_post.create(cr, uid, {'title':vals['name']}, context=context)
        vals.update({'wordpress_post_id': post_id})
        activity_programming_id = super(scout_activity_programming, self).create(cr, uid, vals, context=context)
        return activity_programming_id

    def action_load_template(self, cr, uid, ids, context=None):
        programming_id = ids[0]
        evaluation_id = self.pool.get('scout.activity.programming.evaluation').create(cr, uid, {'activity_programming_id': programming_id})
        wizard_id = self.pool.get('scout.evaluation.template.wizard').create(cr, uid, {'evaluation_id': evaluation_id})
        return {
            'type': 'ir.actions.act_window',
            'name': 'Template selection',
            'res_id': wizard_id,
            'res_model': 'scout.evaluation.template.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'target': 'new'
        }

    def get_units_description(self, cr, uid, ids, name, arg, context):
        """
        Displays the units names
        """
        result = {}
        for programming in self.browse(cr, uid, ids, context=context):
            result[programming.id] = _('Undefined')
            for unit in programming.units_ids:
                result[programming.id] = unit.name

        return result

    def display_attendance_list(self, cr, uid, ids, context=None):
        self_id = ids[0]

        context = context or {}

        context.update({'activity_programming_id': self_id})

        #view_id = self.pool.get('ir.ui.view').search(cr, uid, [('name', '=', 'scout.activity.programming.attendance.form')])
        print(self_id)
        return {
            'type': 'ir.actions.act_window',
            'name': 'Attendance',
            'view_mode': 'kanban,list',
            'view_type': 'kanban',
            # 'view_id': view_id,
            'res_model': 'scout.activity.programming.attendance',
            'target': 'current',
            'context': context,
            'domain': [('activity_programming_id', '=', self_id)]
        }

    def display_confirmation_list(self, cr, uid, ids, context=None):
        self_id = ids[0]
        view_id = self.pool.get('ir.ui.view').search(cr, uid, [('name', '=', 'scout.activity.programming.confirmation.form')])
        return {
            'type': 'ir.actions.act_window',
            'name': 'Confirmation',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': view_id,
            'res_model': 'scout.activity.programming',
            'res_id': self_id,
            'target': 'new',
            'nodestroy': True,
            'context': context
        }

    def publish_article_on_web(self, cr, uid, ids, context=None):
        for record in self.browse(cr, uid, ids, context=context):
            # wordpress_obj = self.pool.get('wordpress.post')
            # wordpress_obj.write(cr, uid, record.wordpress_post_id.id, {'title': record.name})
            record.wordpress_post_id.publishPost()
            self.write(cr, uid, ids, {'web_publication_status': 'published'}, context=context)

        return {'warning': {
            'title': _('Article published'),
            'message': _('The article has been published correctly.')}
        }

    def on_change_name(self, cr, uid, ids, name, context=None):
        for record in self.browse(cr, uid, ids, context=context):
            wordpress_obj = self.pool.get('wordpress.post')
            wordpress_obj.write(cr, uid, record.wordpress_post_id.id, {'title': name})
            # raise openerp.exceptions.Warning(_('Record updated'))
        return True

    def on_change_units(self, cr, uid, ids, units_ids, context=None):

        units_obj = self.pool.get('scout.unit')
        units = units_ids[0][2]

        units_items = []
        for unit in units_obj.browse(cr, uid, units, context=context):
            units_items[len(units_items):] = [unit.age_id.name]

        units_tags = ', '.join(units_items)
        print units_tags

        for record in self.browse(cr, uid, ids, context=context):
            wordpress_obj = self.pool.get('wordpress.post')
            wordpress_obj.write(cr, uid, record.wordpress_post_id.id, {'category': units_tags})

        return True

    def toggle_publication_fields(self, cr, uid, ids, context=None):
        return True

    def activity_evaluated(self, cr, uid, ids, context=None):
        _logger.warning("Activity Evaluated - Changed to evaluated")
        self.write(cr, uid, ids, {'state': 'evaluated'}, context=context)
        return True

    def activity_draft(self, cr, uid, ids, context=None):
        _logger.warning("Activity Draft - Changed to draft")
        self.write(cr, uid, ids, {'state': 'draft'}, context=context)
        return True

    def activity_done(self, cr, uid, ids, context=None):
        for programming in self.browse(cr, uid, ids, context):
            now = datetime.datetime.now()
            _logger.warning(programming.ending_date)
            activity_date_str = strptime(programming.ending_date, "%Y-%m-%d %H:%M:%S")
            activity_date = datetime.datetime(activity_date_str[0], activity_date_str[1], activity_date_str[2], activity_date_str[3], activity_date_str[4], activity_date_str[5])
            if activity_date > now:
                _logger.warning("No activity")
                return False

        _logger.warning("Activity Done - Changed to done")
        self.write(cr, uid, ids, {'state': 'done'}, context=context)
        return True

    def activity_programmed(self, cr, uid, ids, context=None):

        participants_obj = self.pool.get('scout.participant')
        attendance_obj = self.pool.get('scout.activity.programming.attendance')

        for record in self.browse(cr, uid, ids, context=context):
            if record.control_attendance:
                programming_participants_ids = map(lambda attendance: attendance.participant_id.id, record.attendances_ids)

                for participant_id in participants_obj.search(cr, uid, [('unit_id', 'in', map(lambda(unit): unit.id, record.units_ids))], order='id'):
                    if participant_id not in programming_participants_ids:
                        _logger.warning(participant_id)
                        attendance_obj.create(cr, uid, {'activity_programming_id': record.id, 'participant_id': participant_id})
                        # _logger.warning("Participant->" + participant.name)

        _logger.warning("Activity Programmed - Changed to programmed")
        self.write(cr, uid, ids, {'state': 'programmed'}, context=context)
        return True

    def test_complete(self, cr, uid, ids, *args):
        # raise openerp.exceptions.Warning(_('This activity programming is not complete, make sure you have filled the objectives, description, activities and the new for the web.'))
        return True

    def search(self, cr, uid, args, offset=0, limit=None, order=None, context={}, count=False):
        if 'list_my_unit' in context.keys():
            obj_scouter = self.pool.get('scout.scouter')
            scouter_ids = obj_scouter.search(cr, uid, [('user_id', '=', uid)])

            for scouter in obj_scouter.browse(cr, uid, scouter_ids, context=context):
                _logger.warn(args)
                args.append(('units_ids', '=', scouter.unit_id.id))

        return super(scout_activity_programming, self).search(cr, uid, args, offset, limit, order, context, count)

    def get_raw_description(self, cr, uid, ids, name, arg, context):
        """
        Escape the html code for the report
        """
        result = {}
        for activity in self.browse(cr, uid, ids, context=context):
            h = html2text.HTML2Text()
            h.ignore_links = True
            h.ignore_images = True
            h.ignore_emphasis = True
            parsed = h.handle(activity.description)
            result[activity.id] = parsed
        return result

    _order = 'starting_date desc'

    _track = {
        'publish_on_web': {}
    }

    _columns = {
        'name': fields.char("Name", size=128, readonly=True, states={'draft': [('readonly', False)]},),
        'starting_date': fields.datetime("Starting date", readonly=True, states={'draft':[('readonly',False)]}),
        'ending_date': fields.datetime("Ending date", readonly=True, states={'draft':[('readonly',False)]}),
        'objectives_ids': fields.one2many('scout.activity.programming.objective', 'activity_programming_id', 'Objectives', readonly=True, states={'draft': [('readonly', False)]}),
        'description': fields.text('Description', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='onchange'),
        'raw_description': fields.function(get_raw_description, type="text", method=True, string='Raw Description'),
        'control_attendance': fields.boolean("Control attendance", readonly=True, states={'draft': [('readonly', False)]}),
        'publish_on_web': fields.boolean("Publish on Web", readonly=True, states={'draft': [('readonly', False)]}),
        'wordpress_post_id': fields.many2one('wordpress.post', 'Wordpress post'),
        'web_publication_url': fields.related('wordpress_post_id', 'post_url', type='char', size=256, string='Post link', readonly=True),
        'web_publication_date': fields.related('wordpress_post_id', 'publication_date', readonly=True, states={'programmed':[('readonly',False)]}, type='datetime', string='Publication date'),
        'web_publication_status': fields.selection([('pending', 'Pending'),('published', 'Published')],'Publication status',readonly=True),
        'web_publication_description': fields.related('wordpress_post_id', 'content',
                  readonly=True, states={'programmed':[('readonly',False)]}, type='text', string='Web description'),
        'units_description': fields.function(get_units_description, type="text", method=True, string='Units'),
        'units_ids': fields.many2many('scout.unit', 'scout_activity_unit_rel', 'activity_id', 'unit_id', 'Participant units', readonly=True, states={'draft': [('readonly', False)]}),
        'attachment_ids': fields.many2many('ir.attachment', 'scout_activity_programming_attachments', 'activity_programming_id', 'attachment_id', 'Attachments'),
        'activities_ids': fields.many2many('scout.activity', 'scout_activities_programming_rel', 'activity_programming_id', 'activity_id', 'Activities', readonly=True, states={'draft': [('readonly', False)]}),
        'attendances_ids': fields.one2many('scout.activity.programming.attendance', 'activity_programming_id', 'Attendance'),
        'evaluations_ids': fields.one2many('scout.activity.programming.evaluation', 'activity_programming_id', 'Evaluations'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('programmed', 'Programmed'),
            ('done', 'Done'),
            ('evaluated', 'Evaluated'),
            ('cancelled', 'Cancelled')],
            'Status'),
    }
    _defaults = {
        'control_attendance': True,
        'web_publication_status': 'pending',
        'state': 'draft',
        'publish_on_web': True
    }


class scout_activity_programming_attendance(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.activity.programming.attendance'
    _description = 'Scout Activity Programming Attendance'

    def attendance_yes(self, cr, uid, ids, context=None):
        new_value = {'attendance': 'attended'}
        self.write(cr, uid, ids, new_value, context=context)
        # return {'value': new_value, 'nodestroy': True, 'type': 'ir.actions.act_window', 'view_mode': 'form'}
        return False

    def attendance_no(self, cr, uid, ids, context=None):
        new_value = {'attendance': 'not_attended'}
        self.write(cr, uid, ids, new_value, context=context)
        return False

    def confirm_wont_attendance(self, cr, uid, ids, context=None):
        new_value = {'confirmation': 'not_attend'}
        self.write(cr, uid, ids, new_value, context=context)
        return False

    def confirm_will_attendance(self, cr, uid, ids, context=None):
        new_value = {'confirmation': 'attend'}
        self.write(cr, uid, ids, new_value, context=context)
        return False

    _columns = {
        'activity_programming_id': fields.many2one('scout.activity.programming', 'Activity', ondelete='cascade'),
        'participant_id': fields.many2one('scout.participant', 'Participant'),
        'family_name': fields.related('participant_id', 'family_id', 'name', readonly=True, type='char'),
        'confirmation': fields.selection([
            ('unconfirmed', 'Unconfirmed'),
            ('attend', 'Will attend'),
            ('not_attend', 'Wont attend')],
            'Conformation'),
        'attendance': fields.selection([
            ('attended', 'Attended'),
            ('not_attended', 'Did not attend')],
            'Attendance'),
        'comment': fields.text('Attendance comment')
    }
    _defaults = {
        'confirmation': 'unconfirmed',
        'attendance': 'attended'
    }


class scout_activity_programming_objective(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.activity.programming.objective'
    _description = 'Scout Activity Programming Objective'
    _columns = {
        'name': fields.char("Name", size=256,),
        'activity_programming_id': fields.many2one('scout.activity.programming', 'Activity programming', ondelete='cascade')
    }

