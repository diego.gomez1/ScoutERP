import openerp.exceptions

from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
import html2text

import logging

_logger = logging.getLogger(__name__)


class scout_activity_programming_evaluation(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.activity.programming.evaluation'
    _description = 'Scout Activity Programming Objective'

    def _select_partner(self, cr, uid, context={}):
        partner_obj = self.pool.get('res.partner')
        partner_ids = partner_obj.search(cr, uid, [('user_id', '=', uid)], context=context)

        return partner_ids[0]

    def get_raw_description(self, cr, uid, ids, name, arg, context):
        """
        Escape the html code for the report
        """
        result = {}
        for activity in self.browse(cr, uid, ids, context=context):
            h = html2text.HTML2Text()
            h.body_width = 0
            h.ignore_links = True
            h.ignore_images = True
            h.ignore_emphasis = True
            parsed = h.handle(activity.description)
            result[activity.id] = parsed
        return result

    _columns = {
        'activity_programming_id': fields.many2one('scout.activity.programming', 'Activity', ondelete='cascade'),
        'author_id': fields.many2one('res.partner', 'Author'),
        'description': fields.text('Description'),
        'raw_description': fields.function(get_raw_description, type="text", method=True, string='Raw Description'),
        'evaluation_items_ids': fields.one2many('scout.evaluation.item.value', 'programming_evaluation_id', 'Items'),
    }
    _defaults = {
        'author_id': lambda self, cr, uid, ctx: self._select_partner(cr, uid, ctx)
    }


class scout_evaluation_item(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.evaluation.item'
    _description = 'Scout evaluation item'
    _rec_name = 'item_title'
    _columns = {
        'item_title': fields.char("Name", size=256,),
        'item_detail': fields.text('Detail'),
    }


class scout_evaluation_template(osv.Model):
    """
    Scout Evaluation Template with a set of items to evaluate
    """
    _name = 'scout.evaluation.template'
    _description = 'Scout evaluation item value'
    _rec_name = 'template_title'
    _columns = {
        'template_title': fields.char("Name", size=256,),
        'description': fields.text('Description'),
        'evaluation_items_ids': fields.many2many('scout.evaluation.item', 'scout_evaluation_template_items_rel', 'evaluation_template_id', 'evaluation_item_id', 'Items'),
        'units_ids': fields.many2many('scout.unit', 'scout_evaluation_template_unit_rel', 'evaluation_template_id', 'unit_id', 'Participant units'),
        'active': fields.boolean('Template active')
    }
    _defaults = {
        'active': True
    }


class scout_evaluation_item_value(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.evaluation.item.value'
    _description = 'Scout evaluation item value'
    _columns = {
        'programming_evaluation_id': fields.many2one('scout.activity.programming.evaluation', 'Evaluation', readonly=True),
        'evaluation_item_id': fields.many2one('scout.evaluation.item', 'Item', readonly=True),
        'quantitative_evaluation': fields.selection([
            (1, 'Very bad'),
            (2, 'Bad'),
            (3, 'Normal'),
            (4, 'Well'),
            (5, 'Very well')],
            'Quantitative evaluation'),
        'comment': fields.text('Comment'),
    }
    _defaults = {
        'quantitative_evaluation': 3
    }

