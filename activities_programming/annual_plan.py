import openerp.exceptions

from openerp import netsvc
from openerp import pooler
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging

_logger = logging.getLogger(__name__)


class scout_annual_plan(osv.Model):
    """
    Scout Activity Programming
    """
    _name = 'scout.annual.plan'
    _description = 'Scout Annual Plan'

    def activity_active(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'active'}, context=context)
        return True

    def activity_evaluated(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'evaluated'}, context=context)
        return True

    def activity_finished(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'finished'}, context=context)
        return True

    def test_complete(self, cr, uid, ids, context=None):
        return True

    _columns = {
        'name': fields.char("Name", size=128, readonly=True, states={'draft': [('readonly', False)]},),
        'starting_date': fields.date("Starting date", readonly=True, states={'draft':[('readonly',False)]}),
        'ending_date': fields.date("Ending date", readonly=True, states={'draft':[('readonly',False)]}),
        'objectives_ids': fields.one2many('scout.annual.plan.objective', 'annual_plan_id', 'Objectives', readonly=True, states={'draft': [('readonly', False)]}),
        'description': fields.text('Description', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='onchange'),
        'unit_id': fields.many2one('scout.unit', 'Participant unit', readonly=True, states={'draft': [('readonly', False)]}),
        'attachment_ids': fields.many2many('ir.attachment', 'scout_annual_plan_attachments', 'activity_programming_id', 'attachment_id', 'Attachments'),
        'evaluations_ids': fields.one2many('scout.annual.plan.evaluation', 'annual_plan_id', 'Evaluations'),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('evaluated', 'Evaluated'),
            ('finished', 'Finished')],
            'Status')
    }
    _defaults = {
        'state': 'draft'
    }


class scout_annual_plan_objective(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.annual.plan.objective'
    _description = 'Scout Activity Programming Objective'
    _columns = {
        'name': fields.char("Name", size=256,),
        'annual_plan_id': fields.many2one('scout.annual.plan', 'Annual Plan', ondelete='cascade')
    }


class scout_annual_plan_evaluation(osv.Model):
    """
    Scout Activity Programming Objectives
    """
    _name = 'scout.annual.plan.evaluation'
    _description = 'Scout Annual Plan Objective'
    _columns = {
        'annual_plan_id': fields.many2one('scout.annual.plan', 'Annual Plan', ondelete='cascade'),
        'author_id': fields.many2one('res.partner', 'Author'),
        'description': fields.text('Description'),
        'evaluation_items_ids': fields.many2many('scout.evaluation.item.value', 'scout_annual_plan_evaluation_item_values', 'annual_plan_id', 'evaluation_item_value_id', 'Items')
    }
    _defaults = {
        'author_id': lambda self, cr, uid, ctx: uid
    }
