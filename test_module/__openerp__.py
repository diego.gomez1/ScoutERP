# -*- coding: utf-8 -*-

{
    'name': 'Test Module',
    'version': '1.0.1',
    'category': 'learning',
    'description': """
Test Module
===========

This module is created just for learning process.
""",
    'author': 'Diego (diegogd@gmail.com)',
    'depends': ["base"],
    'data': ["test_view.xml"],
    'demo': [],
    'installable': True,
    'application': True,
    'images': []
}

