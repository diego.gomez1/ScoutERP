import pooler
from osv import fields,osv
import base64
import time
import vobject

from osv import osv
from tools.translate import _

SCOUTERS_KEY = '__scouters__'

class scout_participant_vcard(osv.osv_memory):

    _name = "scout.participant.vcard"
    _description = "Scout participant vcard"

    def _get_contact_groups(self, cr, uid, context):
        units_obj = self.pool.get('scout.unit')
        ids = units_obj.search(cr, uid, [])
        units = units_obj.browse(cr, uid, ids)
        items = [(SCOUTERS_KEY, _('Scouters'))] + [(unit.id, unit.name) for unit in units]
        return items

    _columns = {
        'name': fields.char('Filename'),
        'data': fields.binary('File', readonly=True),
        'contacts_groups': fields.selection(_get_contact_groups, 'Contacts groups', required=True),
        'state': fields.selection([('choose', 'choose'), ('get', 'get')]),
    }
    _defaults = {
        'state': 'choose',
        'name': 'contacts.vcf',
        'contacts_groups': SCOUTERS_KEY
    }

    def _get_scouters_vcard(self, cr, uid, ids, context={}):
        """
        Export the scouters data
        """
        file_content = ""

        scouter_obj = self.pool.get('scout.scouter')
        scouters_ids = scouter_obj.search(cr, uid, [('state', '!=', 'inactive')])
        for scouter in scouter_obj.browse(cr, uid, scouters_ids, context=context):
            vcard = vobject.vCard()
            i_counter = 0
            o = vcard.add('fn')
            #o.value = participant.name
            o.value = scouter.name

            o = vcard.add('n')
            o.value = vobject.vcard.Name(given=scouter.name)

            categories = [u'Kraal']
            if scouter.unit_id:
                categories.append(scouter.unit_id.name)

            if len(categories) > 0:
                o = vcard.add('categories')
                o.value = categories

            if scouter.birthday:
                o = vcard.add('bday')
                o.value = scouter.birthday

            if scouter.nickname:
                o = vcard.add('nickname')
                o.value = scouter.nickname

            if scouter.phone:
                o = vcard.add('tel')
                o.type_param = 'HOME'
                o.value = scouter.phone

            if scouter.mobile:
                o = vcard.add('tel')
                o.type_param = 'CELL'
                o.value = scouter.mobile

            if scouter.email:
                o = vcard.add('email')
                o.value = scouter.email

            if scouter.street:
                o = vcard.add('adr')
                o.type_param = 'HOME'
                o.value = vobject.vcard.Address(street=scouter.street, city=scouter.city, region=scouter.state_id.name, code=scouter.zip, box="")

            if scouter.image:
                o = vcard.add('photo')
                o.encoding = 'BASE64'
                o.type_param = 'JPG'
                o.value = scouter.image

            file_content += vcard.serialize()

        return file_content

    def _get_unit_vcard(self, cr, uid, ids, unit_id, context={}):
        """
        Export the data from a unit
        """
        file_content = ""

        participant_obj = self.pool.get('scout.participant')
        participant_ids = participant_obj.search(cr, uid, [('unit_id', '=', int(unit_id) ), ('state', '!=', 'unsubscribed')])
        for participant in participant_obj.browse(cr, uid, participant_ids, context=context):
            vcard = vobject.vCard()
            i_counter = 0
            o = vcard.add('fn')
            #o.value = participant.name
            o.value = "%s %s" % (participant.name, participant.family_id.name)

            o = vcard.add('n')
            o.value = vobject.vcard.Name(family=participant.family_id.name, given=participant.name)

            categories = []
            if participant.unit_id:
                categories.append(participant.unit_id.name)

            if participant.subunit_id:
                categories.append(participant.subunit_id.name)

            if len(categories) > 0:
                o = vcard.add('categories')
                o.value = categories

            if participant.birthday:
                o = vcard.add('bday')
                o.value = participant.birthday

            if participant.nickname:
                o = vcard.add('nickname')
                o.value = participant.nickname

            if participant.main_phone:
                o = vcard.add('tel')
                o.type_param = 'HOME'
                o.value = participant.main_phone

            for parent in participant.family_id.parent_ids:
                if parent.phone:
                    i_counter += 1
                    o = vcard.add('item%d.tel' % i_counter)
                    o.value = parent.phone
                    o = vcard.add('item%d.x-ablabel' % i_counter)
                    o.value = parent.name

            if participant.image:
                o = vcard.add('photo')
                o.encoding = 'BASE64'
                o.type_param = 'JPG'
                o.value = participant.image

            file_content += vcard.serialize()

        return file_content


    def act_getfile(self, cr, uid, ids, context={}):
        """
        Action that exports the data into a BOE formated text file
        """
        this = self.browse(cr, uid, ids)[0]

        if this.contacts_groups == SCOUTERS_KEY:
            file_contents = self._get_scouters_vcard(cr, uid, ids, context)
        else:
            file_contents = self._get_unit_vcard(cr, uid, ids, this.contacts_groups, context)

        ##
        ## Generate the file and save as attachment
        file = base64.encodestring(file_contents)

        file_name = _("vcard_report_%s.vcf") % (time.strftime(_("%Y-%m-%d")))

        self.write(cr, uid, ids, {'state': 'get', 'data': file, 'name': file_name}, context=context)

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'scout.participant.vcard',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': this.id,
            'views': [(False, 'form')],
            'target': 'new',
        }