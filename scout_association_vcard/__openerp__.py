# -*- coding: utf-8 -*-
{
    'name': 'Scout Association Vcard',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'Let exporting Scout members, parents, scouters info in vcard',
    'description': """
Scout association vcard
=======================

Let exporting Scout members, parents, scouters info in vcard
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["scout_association"],
    'data': [
        'wizard/scout_participant_vcard_view.xml'
    ],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
}