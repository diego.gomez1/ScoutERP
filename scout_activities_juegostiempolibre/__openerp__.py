# -*- coding: utf-8 -*-
# "l10n_es_partner", "l10n_es_toponyms", "l10n_es_account_asset", "account_voucher", "hr_expense"
{
    'name': 'Activities from Juegostiempolibre',
    'version': '0.1',
    'category': 'Scout application',
    'summary': 'List of activities from juegostiempolibre.com',
    'description': """
Activities from juegotiempolibre.com
==========

This .
""",
    'author': 'Diego Gómez (diegogd@gmail.com)',
    'depends': ["activities_management" ],
    'data': [ "data/activities_juegostiempolibre.xml", "data/scout.activity.csv"],
    'website': "http://scoutmanagement.somostrotamundos.es",
    'demo': [],
    'installable': True,
    'application': True,
    'images': [],
}