from osv import osv,fields


class scout_age(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.age'
    _description = 'Scout Activity Vote'
    _columns = {
        'name': fields.char("Name", size=256,),
        'age_from': fields.integer('Age from'),
        'age_to': fields.integer('Age to')
    }


class scout_unit(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.unit'
    _description = 'Scout Unit'
    _columns = {
        'name': fields.char("Name", size=256,),
        'age_id': fields.many2one('scout.age', 'Age', ondelete='cascade')
    }


class scout_subunit(osv.Model):
    """
    Scout Activity Objectives
    """
    _name = 'scout.unit.subunit'
    _description = 'Scout subunit'
    _columns = {
        'name': fields.char("Name", size=256,),
        'unit_id': fields.many2one('scout.unit', 'Unit', ondelete='cascade')
    }